package com.example.ccsospringhelloworld.greeting;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Arrays;
import java.util.List;

@RestController
public class GreetingController {

    @GetMapping("api/v1/greetings")
    public List<Greeting> getAllGreetings(){
        List<Greeting> greetings = Arrays.asList(
                new Greeting("Aloha","How you greet in Hawaii"),
                new Greeting("Bonjour","How you greet in France"),
                new Greeting("Hello","How you greet in England"),
                new Greeting("Namaste","How you greet in India"),
                new Greeting("Ola", "How you greet in Spain")
        );
        return greetings;
    }
}
