package com.example.ccsospringhelloworld.greeting;

import lombok.*;

@Getter
@Setter
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor

public class Greeting {
    private String text;
    private String note;
}
