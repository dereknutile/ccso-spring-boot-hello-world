package com.example.ccsospringhelloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class CcsoSpringHelloWorldApplication {

	public static void main(String[] args) {
		SpringApplication.run(CcsoSpringHelloWorldApplication.class, args);
		System.out.println("--- CCSO Spring Boot Hello World application started ---");
	}

}
