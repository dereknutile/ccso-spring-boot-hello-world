# CCSO Spring Boot Hello World Application

## Purpose
Simple application to setup a standard Spring Boot project that can connect to a database.

## Todo
- [x] Setup sample project from start.spring.io
- [x] Create project on Gitlab
- [x] Create IntelliJ project and build successfully
- [x] Add database connection
- [x] Add Greeting model
- [x] Add Greeting controller
- [ ] Test URL
- [ ] Write tests
